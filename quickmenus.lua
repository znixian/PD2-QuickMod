-- cache this copy
local QuickMods = xyz.znix.mods.quickmod
local Menus = QuickMods.Menus

-- Add a menu item. Only processed on startup,
--  actually handled by menu/menus.lua
function Menus:AddMenuItem(name, desc, callback)
	local mod = QuickMods._CurrentMod
	
	self._menus[mod] = self._menus[mod] or {}
	local menus = self._menus[mod]
	
	if QuickMods._SetupMode then
		menus[name] = {name=name, desc=desc, callback=callback}
	elseif menus[name] ~= nil then
		menus[name].callback = callback
	end
end
