-- less typing!
local QuickMods = xyz.znix.mods.quickmod
local List = QuickMods._ModList
local action_menus = QuickMods.Menus._menus

--[[
	We setup the global table for our mod, along with some path variables, and a data table.
	We cache the ModPath directory, so that when our hooks are called, we aren't using the ModPath from a
		different mod.
]]
QuickMods._Menu = QuickMods._Menu or {}
local Menu = QuickMods._Menu
local menuNamespace = "xyz.znix.mods.quickmod.ui."

Menu._path = ModPath

Hooks:Add("LocalizationManagerPostInit", "LocalizationManagerPostInit_" .. menuNamespace, function( loc )
	loc:load_localization_file( Menu._path .. "lang/en.txt")
end)

--[[
	Setup our menu callbacks, load our saved data, and build the menu from our json file.
]]
Hooks:Add( "MenuManagerInitialize", "MenuManagerInitialize_" .. menuNamespace , function( menu_manager )
	local scripts_menu_id = menuNamespace .. "script_list" -- this is a string that will not be found elsewhere, as it has a long and unique prefix.
	local actions_menu_id = menuNamespace .. "actions_list" -- this is a string that will not be found elsewhere, as it has a long and unique prefix.
	
	local script_callback_id = scripts_menu_id .. ".on_script_changed"
	local script_toggle_id = scripts_menu_id .. "_enable_"
	
	local action_callback_id = scripts_menu_id .. "on_action"
	local action_toggle_id = scripts_menu_id .. "_action_"
	
	--[[
		Setup our callbacks as defined in our item callback keys, and perform our logic on the data retrieved.
	]]
	MenuCallbackHandler[script_callback_id] = function(self, item)
		local value = (item:value() == "on" and true or false)
		local toggle_id = item:name()
		for _, mod in pairs(List.list) do
			local test_id = script_toggle_id .. mod.name
			if test_id == toggle_id then
				mod.enabled = value
				List:Save()
				QuickMods:refreshHooks(false)
			end
		end
	end
	
	-- scripts menu
	
	Hooks:Add("MenuManagerSetupCustomMenus", "MenuManagerSetupCustomMenus_" .. scripts_menu_id, function( menu_manager, nodes )
		MenuHelper:NewMenu( scripts_menu_id )
	end)
	
	Hooks:Add("MenuManagerPopulateCustomMenus", "MenuManagerPopulateCustomMenus_" .. scripts_menu_id, function(menu_manager, nodes)
		-- log("Mod options: " .. QuickMods.Utils:dump(List.list))
		for _, mod in pairs(List.list) do
			MenuHelper:AddToggle({
				id = script_toggle_id .. mod.name,
				title = mod.name,
				desc = scripts_menu_id .. ".title", --"Enable and Disable Scripts",
				callback = script_callback_id,
				value = mod.enabled,
				menu_id = scripts_menu_id,
				localized = false,
			})
			-- log("Adding mod option " .. mod.name)
		end
	end)
	
	Hooks:Add("MenuManagerBuildCustomMenus", "MenuManagerBuildCustomMenus_" .. scripts_menu_id, function(menu_manager, nodes)
		nodes[scripts_menu_id] = MenuHelper:BuildMenu( scripts_menu_id )
		local menu = nodes.options -- lua_mod_options_menu
		MenuHelper:AddMenuItem(menu, scripts_menu_id, scripts_menu_id .. ".name", scripts_menu_id .. ".desc", "lua_mod_options_menu")
	end)
	
	
	-- actions menu
	
	Hooks:Add("MenuManagerSetupCustomMenus", "MenuManagerSetupCustomMenus_" .. actions_menu_id, function( menu_manager, nodes )
		MenuHelper:NewMenu( actions_menu_id )
	end)
	
	local function addMenuItem(menu, title, i, priority)
		local cb = action_callback_id .. i.val
		MenuHelper:AddButton({
			id = action_toggle_id .. i.val,
			title = title,
			desc = menu.desc,
			callback = cb,
			menu_id = actions_menu_id,
			localized = false,
			priority = priority
		})
		MenuCallbackHandler[cb] = function(self, item)
			local status, err = pcall(function()
				local ingame = game_state_machine:current_state_name() == "ingame_standard"
				menu.callback(ingame)
			end)
		end
		i.val = i.val + 1
	end
	
	Hooks:Add("MenuManagerPopulateCustomMenus", "MenuManagerPopulateCustomMenus_" .. actions_menu_id, function(menu_manager, nodes)
		local i = {val=0}
		for mod, menus in pairs(action_menus) do
			for _, menu in pairs(menus) do
				addMenuItem(menu, mod.name .. ":" .. menu.name, i, 0)
			end
		end
		addMenuItem({
			desc = "Reload all scripts",
			callback = function(ingame) QuickMods:refreshHooks(false) end
		}, "Reload scripts", i, 10)
	end)
	
	Hooks:Add("MenuManagerBuildCustomMenus", "MenuManagerBuildCustomMenus_" .. actions_menu_id, function(menu_manager, nodes)
		nodes[actions_menu_id] = MenuHelper:BuildMenu( actions_menu_id )
		local menu = nodes.options -- lua_mod_options_menu
		MenuHelper:AddMenuItem(menu, actions_menu_id, actions_menu_id .. ".name", actions_menu_id .. ".desc", scripts_menu_id)
	end)
	
end)
