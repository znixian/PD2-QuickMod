
-- Include Guard --
if
		xyz == nil or
		xyz.znix == nil or
		xyz.znix.mods == nil or
		xyz.znix.mods.quickmod == nil then
	dofile(ModPath .. "main-init.lua")
  
	dofile(ModPath .. "modlist.lua")
	dofile(ModPath .. "utils.lua")
	dofile(ModPath .. "quickmenus.lua")
  
	dofile(ModPath .. "main.lua")
end

local QuickMods = xyz.znix.mods.quickmod
local filename = RequiredScript:lower()

-- skip files we are not using
if not QuickMods._FilesOfInterest[filename] then return end

-- hooks
local loads = QuickMods._Mods.Loads
if loads[filename] ~= nil then
	for _, func in ipairs(loads[filename]) do
		func()
	end
end

-- prepaired hooks
local phfs = QuickMods._Mods.PrepairedHookFunctions
if phfs[filename] ~= nil then
	for name, _ in pairs(phfs[filename]) do
		QuickMods:_ForceRegisterHook(name)
	end
end
