-- cache this copy
local QuickMods = xyz.znix.mods.quickmod

-- refresh the status of the mods
function QuickMods:refreshHooks(setup)
	-- can the mods load the base hooks?
	self._SetupMode = setup
	
	-- clear all the normal hooks
	self._Hooks = {}

	-- Load all the mods
	local modsDir = QuickMods._ModList.script_path
	self.Mod = {}
	function self.Mod:Load(file) dofile(self.Location .. "/" .. file) end
	for _, mod in pairs(QuickMods._ModList.list) do
		self._CurrentMod = mod
		self.Mod.Name = mod.name
		self.Mod.Location = modsDir .. mod.name
		
		dofile(self.Mod.Location .. "/main.lua")
	end
	self.Mod = nil
	self._CurrentMod = nil
end

-- hook a function
function QuickMods:OnLoaded(name, func)
	if self._SetupMode then
		name = name:lower()
		self._FilesOfInterest[name] = true
		local loads = self._Mods.Loads
		loads[name] = loads[name] or {}
		table.insert(loads[name], func)
	end
end

-- hook a function
function QuickMods:OnLoadedRegisterHook(file, name, func, hooktype)
	if self._SetupMode then
		file = file:lower()
		self._FilesOfInterest[file] = true
		local phfs = self._Mods.PrepairedHookFunctions
		phfs[file] = phfs[file] or {}
		phfs[file][name] = true
	end
	
	if func then
		self:Hook(name, func, hooktype)
	end
end

-- add a actual hook to a PD2 function
function QuickMods:RegisterHook(name)
	if self._SetupMode then
		self:_ForceRegisterHook(name)
	end
end

-- add a hook, no matter what
function QuickMods:_ForceRegisterHook(name)
-- add a actual hook to a PD2 function
	-- register the global hook, if it is not already.
	if self._HookedFunctions[name] == nil then
		local hook = function(self2, ...)
			return (self._Hooks[name] or self._HookedFunctionsOrig[name])(self2, ...)
		end
		
		local obj = _G
		local parent = nil
		local fnp = nil
		for part in string.gmatch(name, "[^.:]+") do
			parent = obj
			obj = obj[part]
			fnp = part
		end
		
		self._HookedFunctionsOrig[name] = obj
		self._HookedFunctions[name] = hook
		parent[fnp] = hook
	end
end

-- hook a function
function QuickMods:Hook(name, func, hooktype)
  if not self._CurrentMod.enabled then return end
  
	hooktype = hooktype or self.HookType.APPEND
	
	local prev = self._Hooks[name]
	
	self._Hooks[name] = function(self2, ...)
		local actualPrev = prev or self._HookedFunctionsOrig[name]
		return func(self2, actualPrev, ...)
	end
end

-- load the mod info
QuickMods._ModList:Load()

-- load all the mods.
QuickMods:refreshHooks(true)

