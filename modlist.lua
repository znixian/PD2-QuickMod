-- less typing!
local QuickMods = xyz.znix.mods.quickmod
QuickMods._ModList = QuickMods._ModList or {
  list = {},
  _path = ModPath,
  _data_path = SavePath .. "quickmods.conf",
  script_path = "scripts/",
}

local List = QuickMods._ModList

--[[
	A simple save function that json encodes our _data table and saves it to a file.
]]
function List:Save()
  local data = {
    mods = self.list,
    script_path = self.script_path,
  }
  
  -- the json library can't read empty tables
  if next(data.mods) == nil then
    data.mods = nil
  end
  
	local file = io.open( self._data_path, "w+" )
	if file then
		file:write( json.encode( data ) )
		file:close()
	end
end

function List:_LoadFromFile()
  -- load the information about saved mods
  local fileMods = nil
	local file = io.open( self._data_path, "r" )
	if file then
		fileMods = json.decode( file:read("*all") )
		file:close()
  else
    fileMods = {}
	end
  return fileMods
end

-- Load the mod list, from the file and by directory scanning for new mods.
function List:Load()
  local configData = self:_LoadFromFile()
  local fileMods = configData.mods or {}
  
	-- Load all the mods
	for _, modName in ipairs(file.GetDirectories(self.script_path)) do
    self.list[modName] = fileMods[modName] or {
      name = modName,
      enabled = false,
    }
	end
  
  -- make sure the names match up
  for modName, mod in pairs(self.list) do
    mod.name = modName
    -- log(mod.name .. ": " .. tostring(mod.enabled))
	end
  
  -- is a 
  if configData.script_path == nil then
    self.script_path = "scripts/"
    if not file.DirectoryExists(self.script_path) then
      -- the default scripts path does not exist.
      if file.CreateDirectory == nil then
        -- okay, we're on windows. sigh. this is why we can't have nice things.
        self.script_path = self._path .. "scripts/"
      else
        -- just make it, then
        file.CreateDirectory(self.script_path)
      end
    end
    
    -- save it for next time
    self:Save()
  else
    self.script_path = configData.script_path
  end
  
end