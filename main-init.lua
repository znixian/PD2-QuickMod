-- Un-nil-ify --
xyz = xyz or {}
xyz.znix = xyz.znix or {}
xyz.znix.mods = xyz.znix.mods or {}

-- set the mod up
xyz.znix.mods.quickmod = {
	_SetupMode = false,
	_FilesOfInterest = {},
	_Hooks = {},
	_HookedFunctions = {},
	_HookedFunctionsOrig = {},
	_Mods = {
		Loads = {},
		PrepairedHookFunctions = {},
	},
	HookType = {
		APPEND = 1,
		OVERWRITE = 2
	},
	Menus = {
		_menus = {}
	}
}
