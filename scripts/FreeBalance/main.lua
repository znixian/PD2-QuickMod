-- me hates typing
local QuickMods = xyz.znix.mods.quickmod

-- override a function from payday2
QuickMods:OnLoadedRegisterHook("lib/tweak_data/weapontweakdata", "WeaponTweakData:init", function(self, orig, ...)
	-- run the origional function.
	orig(self, ...)
	
	-- change the ammo pickup rate and clip size of the China Puff grenade launcher
	self.china.CLIP_AMMO_MAX = 1
	self.china.AMMO_PICKUP = {0.05, 0.25} -- pickup between 0.05 and 0.25 of a nade when you walk over a enemy ammo box
	
	self.gre_m79.AMMO_PICKUP = {0.35, 0.65} -- buff the pickup rate for the M79 (GL40) grenade launcher
end)

