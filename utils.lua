-- utilites

-- typing
local QuickMods = xyz.znix.mods.quickmod

-- set up Utils namespace
QuickMods.Utils = {}
local Utils = QuickMods.Utils

-- serialize
function Utils:dump(val)
	if type(val) == "table" then
		local str = "{"
		for name, item in pairs(val) do
			str = str .. name .. "=" --self:dump(item) .. ","
		end
		return str .. "}"
	else
		return tostring(val)
	end
end
